import pytest


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    if report.when == 'call':
        # always add url to report
        extra.append(pytest_html.extras.url('http://www.pizzaboy.com/'))
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            # only add additional html on failure
            extra.append(pytest_html.extras.html('<h1>You FAILED!!!</h1>'))
        report.extra = extra

def pytest_configure(config):
    # config._metadata = None
    keys_to_remove = []

    for key in config._metadata:
        if key[:2] == 'CI':
            keys_to_remove += [key]

    for key in keys_to_remove:
        del config._metadata[key]

    config._metadata['custom data'] = 'THIS IS A CUSTOM VARIABLE'
