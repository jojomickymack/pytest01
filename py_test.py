import unittest
import pytest

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])
    if report.when == 'call':
        # always add url to report
        extra.append(pytest_html.extras.url('http://www.pizzaboy.com/'))
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            # only add additional html on failure
            extra.append(pytest_html.extras.html('<div>YOU FAIL!!!</div>'))
        report.extra = extra


def multiply(num1, num2):
    return num1 * num2


class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_numbers_3_4(self):
        self.assertEqual(multiply(3, 4), 120)

    def test_strings_a_3(self):
        self.assertEqual(multiply('a', 3), 'aaa')

if __name__ == '__main__':
    unittest.main()
